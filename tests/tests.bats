
setup() {
    source ../installer
}

@test "can run doCheckPrivilegied for privilegied user detection" {
  run doCheckPrivilegied
  [ "$status" -eq 0 ]
}

@test "can run doCheckDirectories for destination directories creation" {
  run doCheckDirectories
  [ "$status" -eq 0 ]
}

@test "can run doCheck for global checks" {
  run doCheck
  [ "$status" -eq 0 ]
}

@test "can run doPreinstallConfigureOS for configure specific parameters for specific OS" {
  run doPreinstallConfigureOS
  [ "$status" -eq 0 ]
}

@test "can run doPreinstallUpdateSystem for updating the system installed packages" {
  run doPreinstallUpdateSystem
  [ "$status" -eq 0 ]
}

@test "can run doPreinstallConfigurePackage for install rpm dependencies packages" {
  run doPreinstallConfigurePackage
  [ "$status" -eq 0 ]
}

@test "can run doPreinstallConfigureSnap for install snap dependencies packages" {
  run doPreinstallConfigureSnap
  [ "$status" -eq 0 ]
}

@test "can run doPreinstallConfigurePip for install pip dependencies packages" {
  run doPreinstallConfigurePip
  [ "$status" -eq 0 ]
}

@test "can run doPreinstall for pre-install of package and library context" {
  run doPreinstall
  [ "$status" -eq 0 ]
}

@test "can run doPostinstallCleanupPackages for cleaning the packages manager" {
  run doPostinstallCleanupPackages
  [ "$status" -eq 0 ]
}

@test "can run doPostinstallCleanupTempfiles for cleaning the temporary files" {
  run doPostinstallCleanupTempfiles
  [ "$status" -eq 0 ]
}

@test "can run doPostinstall for executing post installation commands" {
  run doPostinstall
  [ "$status" -eq 0 ]
}



# SXCM Installer

Installer of the [startx cluster manager](https://sxcm.readthedocs.io) utility used for deploying Openshift clusters.

More informations on how to use this installer can be found on the [sxcm installation documentation](https://sxcm.readthedocs.io/en/latest/1-installation) and how to use sxcm in the [sxcm quick start](https://sxcm.readthedocs.io/en/latest/1-installation#quick-start).
